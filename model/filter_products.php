<?php

/**
 * Created by PhpStorm.
 * User: jkris
 * Date: 6/9/2017
 * Time: 9:37 AM
 */
class Filter_products extends ObjectModel
{
    public $id;
    public $id_bundlefilter;
    public $id_category ;

    public $id_products;
    public $order;


    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'filter_products',
        'primary' => 'id',
        'multilang' => false,
        'fields' => array(
            //Fields
            'id_bundlefilter' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'id_category' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'id_products' => array('type' => self::TYPE_HTML,  'validate' => 'isCleanHtml'),
            'order' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 255),


        )
    );

    /*-------------------------------------------------------------*/
    /*  CONSTRUCT
    /*-------------------------------------------------------------*/
    public function __construct($id_filter_products= null)
    {
//        Shop::addTableAssociation('filter_products',array('type' => 'shop'));
        parent::__construct($id_filter_products);
    }


}