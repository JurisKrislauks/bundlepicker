<?php
require_once('filter_products.php');

class bundlefilterMod extends ObjectModel
{
    public $id_bundlefilter;
    public $active = 1;

    public $name;
    public $description;
    public $title;
    public $cat_id;
    public $id_bundle;
    public $id_shop_default;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'bundlefilter',
        'primary' => 'id_bundlefilter',
        'multilang' => true,
        'fields' => array(
            //Fields
            'active' => array('type' => self::TYPE_INT, 'validate' => 'isBool'),
            'name' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 255),
            'description' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'title' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 255),
            'cat_id' => array('type' => self::TYPE_STRING, 'required' => true),
            'id_bundle' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'id_shop_default' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
        )
    );

    /*-------------------------------------------------------------*/
    /*  CONSTRUCT
    /*-------------------------------------------------------------*/
    public function __construct($id_bundlepicker = null, $id_lang = null, $id_shop = null)
    {
        Shop::addTableAssociation('bundlepicker', array('type' => 'shop'));
        parent::__construct($id_bundlepicker, $id_lang, $id_shop);
    }

    public function add($autoddate = true, $null_values = false)
    {
        if (!Tools::isSubmit('id_parent')) {
            $this->id_parent = 0;
        }
        $this->id_shop_default = (int)Context::getContext()->shop->id;
        return parent::add();
    }

    public function getFilter_product_cat($id_category)
    {

        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
                  WHERE id_bundlefilter = ' . $this->id_bundlefilter . ' and id_category =' . $id_category;
        if ($row = Db::getInstance()->getRow($sql)) {
            return new Filter_products($row['id']);
        }

    }
    public function getCategories($id_lang = null){
        $arr = [];
        $categories = explode(';',$this->cat_id);
        foreach ( $categories as $catid){
            $arr[] = new Category($catid, $id_lang);
        }
        return $arr;
    }
    public static function getFiltersbypicker($id_bundlepicker){
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'bundlefilter
                  WHERE id_bundle = ' . $id_bundlepicker . ';';
        if ($row = Db::getInstance()->executeS($sql)) {
//            $objects = [];
//            foreach ($row as $obj){
//                $objects[] = new bundlefilterMod($obj['id_bundlefilter']);
//            }
//            return $objects;
            return $row;
        }
        return false;
    }
    public function getFilterValues($id_category){
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
                  WHERE id_bundlefilter = ' . $this->id_bundlefilter . ' and id_category =' . $id_category;
        if ($row = Db::getInstance()->getRow($sql)) {
            $filter_p =new Filter_products($row['id']);
            $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products_filters
                  WHERE id_filter_products = ' . $filter_p->id . ';';
            if ($rows = Db::getInstance()->executeS($sql)) {
               return $rows;

            }
        }
        return false;
    }
    public function getOrder($id_category)
    {
        $order = "";
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
                        WHERE id_bundlefilter = ' . $this->id_bundlefilter . ' and id_category =' . $id_category;
        if ($row = Db::getInstance()->getRow($sql))
            $order = $row['order'];
        return explode(',', $order);
    }

    public function getSelectedProducts($id_category)
    {
        $products = "";
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
                        WHERE id_bundlefilter = ' . $this->id_bundlefilter . ' and id_category =' . $id_category;
        if ($row = Db::getInstance()->getRow($sql))
            $products = $row['id_products'];
        return explode(';', $products);
    }

    public function delete()
    {
        $response = parent::delete();
        //  $this->reorderMenus();

        return $response;
    }


    public static function getAllIcons($limit = -1, $orderBy = false, $orderWay = false, $only_active = false, $showfields = null)
    {
        $a_fields = array();
        $languages = Language::getLanguages();
        $sql = null;
        if ($limit > 0) {
            $sql = 'SELECT *
		        FROM `' . _DB_PREFIX_ . 'bundlepicker`
		        LIMIT ' . $limit . ';';
        } else {
            $sql = 'SELECT *
		        FROM `' . _DB_PREFIX_ . 'bundlepicker`';
        }


        $result = Db::getInstance()->executeS($sql);

        $returnIcons = array();
        $context = Context::getContext();
        $files = scandir(_PS_IMG_DIR_ . "bundlepicker");
        foreach ($result as $rProject) {
            // $returnIcons[$id]
            $project = new bundlepickerMod($rProject['id_bundlepicker'], (int)$context->language->id);
            $prodArr = array();
            $canAdd = false;
            if ($project->id != null)
                $canAdd = true;
            $prodArr['id_bundlepicker'] = $project->id;
            $prodArr['active'] = $project->active;
            $prodArr['name'] = $project->name;
            $prodArr['description'] = $project->description;

            $prodArr['image'] = null;
            foreach ($files as $file) {
                if (preg_match('/^' . $prodArr['id_bundlepicker'] . '.jpg/i', $file) === 1) {
                    $prodArr['image'] = $context->link->getMediaLink(_PS_IMG_ . 'bundlepicker/' . $file);
                }
            }
            if ($canAdd)
                $returnIcons[(int)$rProject['id_bundlepicker']] = $prodArr;
        }
        return $returnIcons;

//        return $result;


    }

    public function getDepth()
    {
        if ($this->id_parent == null) return 0;
        if ($this->id_parent == 0) return 1;
        $depth = 1;
        do {
            $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'bundlepicker
            WHERE id_bundlepicker =' . $this->id_parent . ';';
            if ($row = Db::getInstance()->getRow($sql)) {
                $depth++;
                $parent = $row['id_parent'];
            } else {
                $parent = 0;
            }
        } while ($parent != 0);
        return $depth;

    }

    public static function getBundleDepth($id)
    {
        $obj = new bundlepickerMod($id);
        return $obj->getDepth();


    }

}