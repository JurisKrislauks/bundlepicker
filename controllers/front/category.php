<?php
//@session_start();
//include_once(_PS_MODULE_DIR_.'fieldtestimonials/defined.php');
//include_once(_PS_MODULE_DIR_.'fieldtestimonials/fieldtestimonials.php');
//include_once(_PS_MODULE_DIR_.'fieldtestimonials/classes/FieldTestimonial.php');
//include_once(_PS_MODULE_DIR_.'fieldtestimonials/classes/FieldFileUploader.php');
//include_once(_PS_MODULE_DIR_.'fieldtestimonials/libs/Params.php');
class BundlepickerCategoryModuleFrontController extends ModuleFrontController
{
    public $errors = array();
    public $success;
    public $identifier;

    public function __construct()
    {
        parent::__construct();

        $this->context = Context::getContext();
        $this->name = 'bundlepicker';
        $this->identifier = 'id_bundlepicker';
//        smartyRegisterFunction($this->context->smarty, 'function', 'testimonialpaginationlink', array('FieldtestimonialsViewsModuleFrontController', 'getTestimonialPaginationLink'));
    }

    public function initContent()
    {
        $this->display_column_left = false;
        $this->display_column_right = false;
        parent::initContent();
        $lang = $this->context->language->id;
        if (Tools::getIsset('cat')) {
            $id_category = Tools::getValue('cat');
            $category = new bundlepickerMod($id_category, $lang);
//            $project = new bundlefilterMod(2, Context::getContext()->language->id);

            if(!Validate::isLoadedObject($category)) return false;
            $data = [];
            $subcategories = null;
            if ($category->isParent()) {
                $subcategories = $category->getSubcategories();
                foreach ($subcategories as &$subc) {
                    $subcategory = new bundlepickerMod($subc['id_bundlepicker']);
                    $filters = $subcategory->getFilters();
                    if ($filters) {
                        foreach ($filters as $filter) {
                            $_filter = new bundlefilterMod($filter['id_bundlefilter']);
                            $subc['filters'][] = $_filter->getFieldsLang($lang)[$lang];
                        }
                    }

                    $langA = $subcategory->getFieldsLang($lang);
                    foreach ($langA as $l) {
                        $subc['lang'] = $l;
                    }
                }
                $data[] = $subcategories;
            }
            $this->context->smarty->assign(array(

                'subcategories' => $subcategories

            ));
            $this->setTemplate('bundlepicker.tpl');
        }


    }

    public static function getTestimonialPaginationLink($params, &$smarty)
    {
        $id = Tools::getValue('id');
        if (!isset($params['p']))
            $p = 1;
        else
            $p = $params['p'];
        if (!isset($params['n']))
            $n = 10;
        else
            $n = $params['n'];
        return Context::getContext()->link->getModuleLink(
            'fieldtestimonials',
            'views',
            array(
                'process' => 'view',
                'id' => $id,
                'p' => $p,
                'n' => $n,
            )
        );
    }

    public function listAllTestimoninals()
    {
        $this->addCSS(_MODULE_DIR_ . _FIELD_TESTIMONIAL_FRONT_URL_ . 'css/style.css');
        $this->name = 'testimonials';
        $this->_configs = '';
        $this->addJqueryPlugin('fancybox');
        $image_type = explode('|', $this->module->getParams()->get('type_image'));
        $video_type = explode('|', $this->module->getParams()->get('type_video'));
        $p = Tools::getValue('p', 1);
        $n = Tools::getValue('n', $this->module->getParams()->get('test_limit'));
        $id = Tools::getValue('id', 0);
        if ($id == 0) {
            $alltestimoninals = FieldTestimonial::getAllTestimonials();
            $testimoninals = FieldTestimonial::getAllTestimonials($p, $n);
            $max_page = floor(sizeof($alltestimoninals) / ((int)(Tools::getValue('n') > 0) ? (int)(Tools::getValue('n')) : 10));
            $this->context->smarty->assign(array('alltestimoninals' => $alltestimoninals));
        } else {
            $testimoninals = FieldTestimonial::getAllTestimonials(1, false, $id, false);//view all and curent testimonial
            $other_testimoninals = FieldTestimonial::getAllTestimonials(1, false, false, $id); // view all other testimonial
            $page_other_testimonials = FieldTestimonial::getAllTestimonials($p, $n, false, $id); // item on page
            $this->context->smarty->assign(array('other_testimoninals' => $other_testimoninals));
            $max_page = floor(sizeof($other_testimoninals) / ((int)(Tools::getValue('n') > 0) ? (int)(Tools::getValue('n')) : 10));
            $this->context->smarty->assign(array('other_testimoninals' => $other_testimoninals, 'page_other_testimonials' => $page_other_testimonials));
        }
        $this->context->smarty->assign(array(
            'page' => ((int)(Tools::getValue('p')) > 0 ? (int)(Tools::getValue('p')) : 1),
            'nbpagination' => ((int)(Tools::getValue('n') > 0) ? (int)(Tools::getValue('n')) : $n),
            'nArray' => array(10, 20, 50),
            'max_page' => $max_page,
            'testimoninals' => $testimoninals,
            'id' => $id,
            'image_type' => $image_type,
            'video_type' => $video_type,
            'name' => $this->name,
        ));
        $this->setTemplate('all_testimonials.tpl');
    }

    public function formTestimoninals()
    {
        $this->addCSS(_MODULE_DIR_ . _FIELD_TESTIMONIAL_FRONT_URL_ . 'css/style.css');
        $this->addJS(_PS_JS_DIR_ . 'validate.js');
        $this->addJS(_THEME_JS_DIR_ . 'validate_fields.js');
        $tm_captcha = (int)$this->module->getParams()->get('captcha');
        $captcha_code = _MODULE_DIR_ . 'fieldtestimonials/captcha.php';
        $loader_image = $video_vimeo = _MODULE_DIR_ . _FIELD_TESTIMONIAL_FRONT_URL_ . 'img/loading.gif';
        $this->context->smarty->assign(array(
            'captcha' => $tm_captcha,
            'captcha_code' => $captcha_code,
            'name_post' => html_entity_decode(Tools::getValue('name_post')),
            'company' => html_entity_decode(Tools::getValue('company')),
            'address' => html_entity_decode(Tools::getValue('address')),
            'media_link' => html_entity_decode(Tools::getValue('media_link')),
            'content' => html_entity_decode(Tools::getValue('content')),
            'email' => html_entity_decode(Tools::getValue('email')),
            'errors' => $this->errors,
            'success' => $this->success,
            'loader_image' => $loader_image,
        ));
        $this->setTemplate('form_submit.tpl');
    }

    public function postProcess()
    {
        if (Tools::getIsset('ajax') && Tools::getIsset('getFilterinfo')) {
            $id_lang = $this->context->language->id;
            $bundlefilter = new bundlefilterMod(Tools::getValue('id_bundlefilter'));


            $categories = $bundlefilter->getCategories($id_lang);
            $tplCategories = [];
            $prepareVars = [];
            //@todo Needs main category name
            foreach ($categories as &$category) {
                $prepareVars['id'] = $category->id;
                $prepareVars['name'] = $category->name;
                $prepareVars['enabled'] = false;
                $productsTpl = [];

                if (Category::hasChildren($category->id, $id_lang)) {
                    $child_categories = Category::getChildren($category->id, $id_lang);
                    foreach ($child_categories as $cCat) {
                        $_category = new Category($cCat['id_category'], $id_lang);

                        //Filter info
                        $filter_info = $bundlefilter->getFilterValues($_category->id_category);
                        $selectedProducts = $bundlefilter->getSelectedProducts($_category->id_category);
                        $products = $this->getFilteredProducts($_category, $id_lang, $filter_info, $selectedProducts,$prepareVars['enabled']);

                        $order = $bundlefilter->getOrder($_category->id);


                        $products = $this->sortArrayByArray($products, $order);
                        foreach ($products as $product) {
                            $productsTpl[$product['id_product']] = $product;

                        }

                    }
                } else {
                    //Filter info
                    $filter_info = $bundlefilter->getFilterValues($category->id_category);
                    $selectedProducts = $bundlefilter->getSelectedProducts($category->id_category);
                    $products = $this->getFilteredProducts($category, $id_lang, $filter_info, $selectedProducts,$prepareVars['enabled']);
                    $order = $bundlefilter->getOrder($category->id);
                    $products = $this->sortArrayByArray($products, $order);

                    foreach ($products as $product) {
                        $productsTpl[$product['id_product']] = $product;


                    }
                }
//                $products = $category->getProducts($id_lang, 1, 2, null, null, false, true, false, 1, false);
                $prepareVars['products'] = $productsTpl;
                $tplCategories[] = $prepareVars;
            }
            $this->context->smarty->assign(array(
                'categories' => $tplCategories,
            ));
            $destTpl = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'bundlepicker/views/templates/front/filters.tpl');
            $return = [];
            $return['response'] = $destTpl;
            die(Tools::jsonEncode($return));

        }
    }

    public function getFilteredProducts($_category, $id_lang, $filter_info, $selectedProducts, &$hasAtleastOneProduct)
    {
        $products = $_category->getProducts($id_lang, 1, 2, null, null, false, true, false, 1, false);

        foreach ($products as $key => &$prod) {
            $product = new Product($prod['id_product']);
            $prod['attrgroups'] = $this->assignAttributesGroups($prod['id_product']);
            $prod['attributes'] = $this->assignAttributesCombinations($prod['id_product']);
            $prod['imagData'] = $this->assignImages($prod['id_product']);
            $prod['attachments'] = (($product->cache_has_attachments) ? $product->getAttachments($this->context->language->id) : array());
            if (isset($prod['attrgroups']['combinations']))
                foreach ($prod['attrgroups']['combinations'] as $key2 => &$comb) {
                    $comb['total_price_wreduc'] = Product::getPriceStatic((int)$prod['id_product'], true, $key2, 6, null, false);
                    $comb['total_price_nreduc'] = Product::getPriceStatic((int)$prod['id_product'], true, $key2, 6, null, false, false);
                    $data_custom = $this->getAttributeGroupCustomFields($key2);
                    $comb['name'] = $data_custom['coname'];
                    $comb['qAmountLabel'] = explode(';', $data_custom['qAmountLabel']);
                    $comb['valueperitem_w'] = $comb['qAmountLabel'][0] != 0 ? $comb['total_price_wreduc'] / $comb['qAmountLabel'][0] : 0;
                    $comb['valueperitem_n'] = $comb['qAmountLabel'][0] != 0 ? $comb['total_price_nreduc'] / $comb['qAmountLabel'][0] : 0;
                    $images = $prod['attrgroups']['combinationImages'][$key2];
                    $comb['image'] = $images[0];


                    if ($comb['name'] == "") {
                        $tmpName = $prod['name'] . ' ';
                        foreach ($comb['attributes_values'] as $groupID => $name) {

                            $groupName = isset($prod['attrgroups']['groups'][$groupID]['name']) ? $prod['attrgroups']['groups'][$groupID]['name'] : "";
                            $tmpName .= $groupName . ' - ' . $name . ', ';
                        }
                        $comb['name'] = $tmpName;
                    }
                    //Filtering out based on filter_product_filter

                    //making all attribute string to make big array
                    $bigString = "";
                    if ($filter_info)
                        foreach ($filter_info as $filter) {
                            $bigString .= $filter['attributes'] . ';';
                        }

                    $attribute_array = explode(';', substr($bigString, 0, -1));
                    $is_selected_attribute = false;
                    if (isset($comb['attributes'])) {
                        foreach ($comb['attributes'] as $aID) {
                            if (in_array($aID, $attribute_array)) {
                                $is_selected_attribute = true;
                                break;
                            }
                        }
                    }
                    if (!$is_selected_attribute) {
                        $comb['enabled'] = false;
                    } else {
                        $comb['enabled'] = true;
                        $hasAtleastOneProduct = true;
                    }


                }
            else {
                $prod['attrgroups']['combinations'] = false;
            }
            if ($key == 0) {
                if (isset($prod['attrgroups']['groups']))
                    foreach ($prod['attrgroups']['groups'] as $key2 => $group) {
                        $attributestpl[$key2] = $group['name'];
                    }
            }
            $canDelete = false;
            if ($selectedProducts) {
                if (in_array($prod['id_product'], $selectedProducts))
                    $canDelete = true;
            }

            if ($canDelete) {
                $prod['deleted'] = true;
            } else {
                $prod['deleted'] = false;
            }
            if (Tools::getValue('pr') == $prod['id_product']) {
                $switchable_key = $key;
            }
        }
        return $products;
    }

    public function assignAttributesGroups($prod_id)
    {
        $colors = array();
        $groups = array();
        $product = new Product($prod_id);
        $context = Context::getContext();
        $attributes_groups = $product->getAttributesGroups($context->language->id);
        if (is_array($attributes_groups) && $attributes_groups) {
            $combination_images = $product->getCombinationImages($context->language->id);
            $combination_prices_set = array();
            foreach ($attributes_groups as $k => $row) {
                if (isset($row['is_color_group']) && $row['is_color_group'] && (isset($row['attribute_color']) && $row['attribute_color']) || (file_exists(_PS_COL_IMG_DIR_ . $row['id_attribute'] . '.jpg'))) {
                    $colors[$row['id_attribute']]['value'] = $row['attribute_color'];
                    $colors[$row['id_attribute']]['name'] = $row['attribute_name'];
                    if (!isset($colors[$row['id_attribute']]['attributes_quantity'])) {
                        $colors[$row['id_attribute']]['attributes_quantity'] = 0;
                    }
                    $colors[$row['id_attribute']]['attributes_quantity'] += (int)$row['quantity'];
                }
                if (!isset($groups[$row['id_attribute_group']])) {
                    $groups[$row['id_attribute_group']] = array(
                        'group_name' => $row['group_name'],
                        'name' => $row['public_group_name'],
                        'group_type' => $row['group_type'],
                        'default' => -1,
                    );
                }
                $groups[$row['id_attribute_group']]['attributes'][$row['id_attribute']] = $row['attribute_name'];
                if ($row['default_on'] && $groups[$row['id_attribute_group']]['default'] == -1) {
                    $groups[$row['id_attribute_group']]['default'] = (int)$row['id_attribute'];
                }
                if (!isset($groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']])) {
                    $groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] = 0;
                }
                $groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] += (int)$row['quantity'];
                $combinations[$row['id_product_attribute']]['attributes_values'][$row['id_attribute_group']] = $row['attribute_name'];
                $combinations[$row['id_product_attribute']]['attributes'][] = (int)$row['id_attribute'];
                $combinations[$row['id_product_attribute']]['price'] = (float)Tools::convertPriceFull($row['price'], null, Context::getContext()->currency, false);
                if (!isset($combination_prices_set[(int)$row['id_product_attribute']])) {
                    Product::getPriceStatic((int)$product->id, false, $row['id_product_attribute'], 6, null, false, true, 1, false, null, null, null, $combination_specific_price);
                    $combination_prices_set[(int)$row['id_product_attribute']] = true;
                    $combinations[$row['id_product_attribute']]['specific_price'] = $combination_specific_price;
                }
                $combinations[$row['id_product_attribute']]['ecotax'] = (float)$row['ecotax'];
                $combinations[$row['id_product_attribute']]['weight'] = (float)$row['weight'];
                $combinations[$row['id_product_attribute']]['quantity'] = (int)$row['quantity'];
                $combinations[$row['id_product_attribute']]['reference'] = $row['reference'];
                $combinations[$row['id_product_attribute']]['unit_impact'] = Tools::convertPriceFull($row['unit_price_impact'], null, Context::getContext()->currency, false);
                $combinations[$row['id_product_attribute']]['minimal_quantity'] = $row['minimal_quantity'];
                if ($row['available_date'] != '0000-00-00' && Validate::isDate($row['available_date'])) {
                    $combinations[$row['id_product_attribute']]['available_date'] = $row['available_date'];
                    $combinations[$row['id_product_attribute']]['date_formatted'] = Tools::displayDate($row['available_date']);
                } else {
                    $combinations[$row['id_product_attribute']]['available_date'] = $combinations[$row['id_product_attribute']]['date_formatted'] = '';
                }
                if (!isset($combination_images[$row['id_product_attribute']][0]['id_image'])) {
                    $combinations[$row['id_product_attribute']]['id_image'] = -1;
                } else {
                    $combinations[$row['id_product_attribute']]['id_image'] = $id_image = (int)$combination_images[$row['id_product_attribute']][0]['id_image'];
                    if ($row['default_on']) {
                        if ($id_image > 0) {
                            if (isset($context->smarty->tpl_vars['images']->value)) {
                                $product_images = $context->smarty->tpl_vars['images']->value;
                            }
                            if (isset($product_images) && is_array($product_images) && isset($product_images[$id_image])) {
                                $product_images[$id_image]['cover'] = 1;
                                $context->smarty->assign('mainImage', $product_images[$id_image]);
                                if (count($product_images)) {
                                    $context->smarty->assign('images', $product_images);
                                }
                            }
                            if (isset($context->smarty->tpl_vars['cover']->value)) {
                                $cover = $context->smarty->tpl_vars['cover']->value;
                            }
                            if (isset($cover) && is_array($cover) && isset($product_images) && is_array($product_images)) {
                                $product_images[$cover['id_image']]['cover'] = 0;
                                if (isset($product_images[$id_image])) {
                                    $cover = $product_images[$id_image];
                                }
                                $cover['id_image'] = (Configuration::get('PS_LEGACY_IMAGES') ? ($product->id . '-' . $id_image) : (int)$id_image);
                                $cover['id_image_only'] = (int)$id_image;
                                $context->smarty->assign('cover', $cover);
                            }
                        }
                    }
                }
            }
            if (!Product::isAvailableWhenOutOfStock($product->out_of_stock) && Configuration::get('PS_DISP_UNAVAILABLE_ATTR') == 0) {
                foreach ($groups as &$group) {
                    foreach ($group['attributes_quantity'] as $key => &$quantity) {
                        if ($quantity <= 0) {
                            unset($group['attributes'][$key]);
                        }
                    }
                }
                foreach ($colors as $key => $color) {
                    if ($color['attributes_quantity'] <= 0) {
                        unset($colors[$key]);
                    }
                }
            }
            foreach ($combinations as $id_product_attribute => $comb) {
                $attribute_list = '';
                foreach ($comb['attributes'] as $id_attribute) {
                    $attribute_list .= '\'' . (int)$id_attribute . '\',';
                }
                $attribute_list = rtrim($attribute_list, ',');
                $combinations[$id_product_attribute]['list'] = $attribute_list;
            }
            return array(
                'groups' => $groups,
                'colors' => (count($colors)) ? $colors : false,
                'combinations' => $combinations,
                'combinationImages' => $combination_images
            );
        }
    }

    public function getAttributeGroupCustomFields($id_product_attribute)
    {
        $combination = new Combination($id_product_attribute);
        $data['qAmountLabel'] = isset($combination->qAmountLabel) ? $combination->qAmountLabel : '';
        $data['coname'] = isset($combination->coname) ? $combination->coname : '';
        return $data;
    }

    public function assignImages($id_product)
    {
        $product = new Product($id_product);
        $context = Context::getContext();
        $images = $product->getImages((int)$context->cookie->id_lang);
        $product_images = array();
        if (isset($images[0])) {
            $context->smarty->assign('mainImage', $images[0]);
        }
        foreach ($images as $k => $image) {
            if ($image['cover']) {
                $context->smarty->assign('mainImage', $image);
                $cover = $image;
                $cover['id_image'] = (Configuration::get('PS_LEGACY_IMAGES') ? ($product->id . '-' . $image['id_image']) : $image['id_image']);
                $cover['id_image_only'] = (int)$image['id_image'];
            }
            $product_images[(int)$image['id_image']] = $image;
        }
        if (!isset($cover)) {
            if (isset($images[0])) {
                $cover = $images[0];
                $cover['id_image'] = (Configuration::get('PS_LEGACY_IMAGES') ? ($product->id . '-' . $images[0]['id_image']) : $images[0]['id_image']);
                $cover['id_image_only'] = (int)$images[0]['id_image'];
            } else {
                $cover = array(
                    'id_image' => $context->language->iso_code . '-default',
                    'legend' => 'No picture',
                    'title' => 'No picture'
                );
            }
        }
        $size = Image::getSize(ImageType::getFormatedName('large'));
        return array(
            'have_image' => (isset($cover['id_image']) && (int)$cover['id_image']) ? array((int)$cover['id_image']) : Product::getCover((int)Tools::getValue('id_product')),
            'cover' => $cover,
            'imgWidth' => (int)$size['width'],
            'mediumSize' => Image::getSize(ImageType::getFormatedName('medium')),
            'largeSize' => Image::getSize(ImageType::getFormatedName('large')),
            'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
            'cartSize' => Image::getSize(ImageType::getFormatedName('cart')),
            'col_img_dir' => _PS_COL_IMG_DIR_,
            'product_images' => $product_images);
    }

    public function assignAttributesCombinations($prod_id)
    {
        $attributes_combinations = Product::getAttributesInformationsByProduct($prod_id);
        if (is_array($attributes_combinations) && count($attributes_combinations)) {
            foreach ($attributes_combinations as &$ac) {
                foreach ($ac as &$val) {
                    $val = str_replace(Configuration::get('PS_ATTRIBUTE_ANCHOR_SEPARATOR'), '_', Tools::link_rewrite(str_replace(array(',', '.'), '-', $val)));
                }
            }
        } else {
            $attributes_combinations = array();
        }
        return array(
            'attributesCombinations' => $attributes_combinations,
            'attribute_anchor_separator' => Configuration::get('PS_ATTRIBUTE_ANCHOR_SEPARATOR')
        );
    }

    public function sortArrayByArray(array $array, array $orderArray)
    {
        $ordered = array();

        foreach ($orderArray as $key) {
            foreach ($array as $pkey => $product) {
                if ($product['id_product'] == $key) {
                    $ordered[] = $product;
                    unset($array[$pkey]);
                }
            }
        }
        return $ordered + $array;
    }


}
