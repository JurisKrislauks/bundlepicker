<?php

class AdminBundleFilterController extends ModuleAdminController
{
    public $imageType = 'jpg';
    protected $_bundlefilter = null;

    public function __construct()
    {
        $this->__nameO = 'bundlefilter';
        $this->className = $this->__nameO . 'Mod';
        $this->table = $this->__nameO;
        $this->meta_title = $this->l('Filters');
        $this->deleted = false;
        $this->explicitSelect = true;
        $this->context = Context::getContext();
        $this->lang = true;
        $this->bootstrap = true;
        $this->meta_title = array($this->l('Filters'));
        $this->module = 'bundlepicker';
        if (!Tools::getIsset('id_bundle') && !Tools::getIsset('viewbundlefilter') &&
            !Tools::getIsset('ajax') && !Tools::getIsset('id_bundlefilter')
        ) {
            $link = $this->context->link->getAdminLink('AdminBundlePicker');
            Tools::redirectAdmin($link);
        }

        if (Shop::isFeatureActive()) {
            Shop::addTableAssociation($this->table, array('type' => 'shop'));
        }

        $this->position_identifier = 'id_' . $this->__nameO;

        $this->addRowAction('edit');
        $this->addRowAction('view');
        $this->addRowAction('delete');

        $this->fieldImageSettings = array(
            'name' => 'logo',
            'dir' => $this->__nameO
        );
        $this->fields_list = array(
            'id_' . $this->__nameO => array(
                'title' => $this->l('ID'),
                'type' => 'int',
                'class' => 'fixed-width-xs',
                'orderby' => false
            ),
            'image' => array(
                'title' => $this->l('Logo'),
                'image' => $this->__nameO,
                'orderby' => false,
                'search' => false,
                'align' => 'center',

            ),
            'title' => array(
                'title' => $this->l('Title'),
                'align' => 'center',
                'orderby' => false
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'width' => 'auto',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false,
                'class' => 'fixed-width-xs',
            ),


        );

        parent::__construct();


    }

    public function initToolBarTitle()
    {
        $bundle = new bundlepickerMod(Tools::getValue('id_bundle'), Context::GetContext()->language->id);
        $this->toolbar_title[] = $this->l($bundle->name);
        $this->toolbar_title[] = $this->l('Filters');
    }
    /* ------------------------------------------------------------- */
    /*  INIT PAGE HEADER TOOLBAR
    /* ------------------------------------------------------------- */
    public function initPageHeaderToolbar()
    {

        if (empty($this->display)) {
            $this->page_header_toolbar_btn = array(
                'new' => array(
                    'href' => self::$currentIndex . '&add' . $this->__nameO . '&token=' . $this->token,
                    'desc' => $this->l('Add New Category', null, null, false),
                    'icon' => 'process-icon-new'
                )
            );
        }

        parent::initPageHeaderToolbar();
    }


    public function renderForm()
    {

        if (!($object = $this->loadObject(true))) {
            return;
        }

        $image = _PS_IMG_DIR_ . $this->__nameO . '/' . $object->id . '.jpg';
        $image_url = ImageManager::thumbnail($image, $this->table . '_' . (int)$object->id . '.' . $this->imageType, 350,
            $this->imageType, true, true);
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;

        // Init Fields form array
        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Menu'),
                'icon' => 'icon-cogs'
            ),
            // Inputs
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Filter Title'),
                    'name' => 'title',
                    'desc' => $this->l('Must be less than 125 characters.'),
                    'required' => true,
                    'lang' => false
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Filter Name'),
                    'name' => 'name',
                    'desc' => $this->l('Must be less than 250 characters.'),
                    'required' => true,
                    'lang' => true
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description'),
                    'name' => 'description',

                    'autoload_rte' => 'rte', //Enable TinyMCE editor for description
                    'required' => false,
                    'lang' => true
                ),

                array(
                    'type' => 'file',
                    'label' => $this->l('Logo'),
                    'name' => 'logo',
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'display_image' => true,
                    'col' => 6,
                    'hint' => $this->l('Upload a Pretty image logo from your computer.')
                ),
                array(

                    'type' => 'categories',
                    'label' => $this->l('Product categories'),
                    'name' => 'cat_id',
                    'tree' => array(
                        'id' => 'id_root_bundlepicker',
                        'use_checkbox' => true,
                        'selected_categories' => explode(';', $object->cat_id),
                    ),

                ),

                array(
                    'type' => 'hidden',
                    'name' => 'id_bundle',
                    'value' => Tools::getValue('id_bundle')

                ),

            ),
            // Submit Button
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'saveBundleFilterroot'
            )
        );
        $id_parent = 0;
        $id_parent = Tools::getValue('id_parent');

//        $depth = bundlepickerMod::getBundleDepth($id_parent);
//        if($depth>0){
//            $this->fields_form['input'][]=array(
//
//                    'type' => 'categories',
//                    'label' => $this->l('Categories 2'),
//                    'name' => 'cat_ids',
//                    'tree' => array(
//                        'id' => 'id_root_bundlepicker2',
//                        'use_checkbox' => true,
//                        //'selected_categories' => explode(';', $object->cat_id),
//                    ),
//
//            );
//        }else{
//            $this->fields_form['input'][]=array(
//
//                'type' => 'categories',
//                'label' => $this->l('Category'),
//                'name' => 'cat_id',
//                'tree' => array(
//                    'id' => 'id_root_bundlepicker',
//                    'use_checkbox' => false,
//                    'selected_categories' => explode(';', $object->cat_id),
//                ),
//
//            );
//        }
        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
            );
        }

        return parent::renderForm();
    }

    public function renderList()
    {
        if (isset($this->_filter) && trim($this->_filter) == '') {
            $this->_filter = $this->original_filter;
        }

        $this->addRowAction('view');
        $this->addRowAction('add');
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        return parent::renderList();
    }

    public function init()
    {
        parent::init();

        // context->shop is set in the init() function, so we move the _bundlepicker instanciation after that
        if (($id_bundlefilter = Tools::getvalue('id_' . $this->__nameO)) && $this->action != 'select_delete') {
            $this->_bundlefilter = new bundlefilterMod($id_bundlefilter);
        }

        $id_bundle = 0;
        if (Tools::isSubmit('id_bundle')) {
            $id_bundle = Tools::getValue('id_bundle');
        }
        if (Tools::getIsset('viewbundlefilter') || Tools::getIsset('id_bundlefilter'))
            return $this->renderView();
        $this->_select = 'a.`id_bundle`';
        $this->original_filter = $this->_filter .= ' AND `id_bundle` = ' . (int)$id_bundle . ' ';
        $this->_use_found_rows = false;

        $this->_join .= ' LEFT JOIN `' . _DB_PREFIX_ . 'bundlefilter_shop` sa ON (a.`id_bundlefilter` = sa.`id_bundlefilter` AND sa.id_shop = a.id_shop_default) ';

    }

    public function renderView()
    {
        $languages = $this->context->language->getLanguages(false);
        $id_lang = $this->context->language->id;
        $iso = $this->context->language->iso_code;

        $id_object = Tools::getValue('id_' . $this->__nameO);
        $ajax_url = $this->context->link->getAdminLink('AdminBundleFilter');
        $id_bundlefilter = Tools::GetValue('id_bundlefilter');
        $bundleFilter = new bundlefilterMod($id_bundlefilter);
        $categories = array();
        foreach (explode(';', $bundleFilter->cat_id) as $cat_id) {
            $category = new Category($cat_id, $id_lang);
            if (Validate::isLoadedObject($category)) {
                $categories[] = $category;
            }
        };

        $this->tpl_view_vars = array(
            'languages' => $languages,
            'categories' => $categories,
            'id_' . $this->__nameO => $id_object,
            'ajax_url_id_' . $this->__nameO => $ajax_url,

        );
        $this->base_tpl_view = 'filters.tpl';
        return parent::renderView();

    }

    public function initToolbar()
    {
        $id_bundle = Tools::getValue('id_bundle');
        if (empty($this->display)) {
            $this->toolbar_btn['new'] = array(
                'href' => self::$currentIndex . '&add' . $this->table . '&token=' . $this->token . '&id_bundle=' . $id_bundle,
                'desc' => $this->l('Add New')
            );

            if ($this->can_import) {
                $this->toolbar_btn['import'] = array(
                    'href' => $this->context->link->getAdminLink('AdminImport', true) . '&import_type=categories',
                    'desc' => $this->l('Import')
                );

            }


        }

        if (Tools::getValue('id_bundlefilter') && !Tools::isSubmit('updatefilter')) {
            $this->toolbar_btn['edit'] = array(
                'href' => self::$currentIndex . '&update' . $this->table . '&id_bundlefilter=' . (int)Tools::getValue('id_bundlefilter') . '&token=' . $this->token,
                'desc' => $this->l('Edit')
            );
        }

        if ($this->display == 'view') {
            $this->toolbar_btn['new'] = array(
                'href' => self::$currentIndex . '&add' . $this->table . '&id_bundle=' . (int)Tools::getValue('id_bundlefilter') . '&token=' . $this->token,
                'desc' => $this->l('Add New')
            );
        }
        parent::initToolbar();

        // after adding a category
        if (empty($this->display)) {
            $id_bundlefilter = (Tools::isSubmit('id_bundlefilter')) ? '&id_parent=' . (int)Tools::getValue('id_bundlefilter') : '';
            $this->toolbar_btn['new'] = array(
                'href' => self::$currentIndex . '&add' . $this->table . '&token=' . $this->token . $id_bundlefilter . '&id_bundle=' . $id_bundle,
                'desc' => $this->l('Add New')
            );

            if (Tools::isSubmit('id_bundlefilter')) {
                $back = Tools::safeOutput(Tools::getValue('back', ''));
                if (empty($back)) {
                    $back = self::$currentIndex . '&token=' . $this->token;
                }
                $this->toolbar_btn['back'] = array(
                    'href' => $back,
                    'desc' => $this->l('Back to list')
                );
            }
        }
    }

    public function processAdd()
    {
//        $id_category = (int)Tools::getValue('id_category');
        $id_bundle = (int)Tools::getValue('id_bundle');

        // if true, we are in a root category creation
        if (!$id_bundle) {

            $_POST['id_bundle'] = $id_bundle = 0;
        }
        $categories = Tools::getValue('cat_id');
        $categories_string = implode(';', $categories);
        $_POST['cat_id'] = $categories_string;
        $object = parent::processAdd();

        $id_bundlefilter = $object->id;
        foreach ($categories as $category) {
            Db::getInstance()->insert('filter_products', array(
                'id_bundlefilter' => (int)$id_bundlefilter,
                'id_category' => (int)$category,
                'id_products' => '',
                'order' => ''
            ));
        }


        return $object;
    }

    public function processUpdate()
    {

        $id_bundle = (int)Tools::getValue('id_bundle');

        // if true, we are in a root category creation
        if (!$id_bundle) {

            $_POST['id_bundle'] = $id_bundle = 0;
        }
        $categories = Tools::getValue('cat_id');
        $categories_string = implode(';', $categories);
        $_POST['cat_id'] = $categories_string;


        //deleting filter_products which were deleted upon update
        $id_bundlefilter = (int)Tools::getValue('id_bundlefilter');
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
                        WHERE id_bundlefilter = ' . $id_bundlefilter;
        if ($row = Db::getInstance()->executeS($sql)) {
            foreach ($row as $filter_product) {
                if (!in_array($filter_product['id_category'], $categories)) {

                    //deleting filter_product_filters
                    Db::getInstance()->delete('filter_products_filters', 'id_filter_products = ' . $filter_product['id'], 0);


                    Db::getInstance()->delete('filter_products', 'id = ' . $filter_product['id'], 0);
                }
            }

        };


        $object = parent::processUpdate();


        return $object;
    }

    public function processDelete()
    {
        $id_bundlefilter = (int)Tools::getValue('id_bundlefilter');
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
                        WHERE id_bundlefilter = ' . $id_bundlefilter;
        if ($row = Db::getInstance()->executeS($sql)) {
            foreach ($row as $filter_product) {
                //deleting filter_product_filters
                Db::getInstance()->delete('filter_products_filters', 'id_filter_products = ' . $filter_product['id'], 0);

            }
            Db::getInstance()->delete('filter_products', 'id_bundlefilter = ' . $id_bundlefilter, 0);
        }
        $object = parent::processDelete();


        return $object;
    }

    public function postProcess()
    {
        if ($this->_postValidation()) {

            $lang_id = $this->context->language->id;
            if (Tools::getIsset('getCategoryinfo')) {

                $category = new Category(Tools::getValue('catID'), $lang_id);
                $ajax_url = $this->context->link->getAdminLink('AdminBundleFilter');
                $id_bundlefilter = Tools::getValue('id_bundlefilter');
                if (Category::hasChildren($category->id, $this->context->language->id)) {
                    $child_categories = Category::getChildren($category->id, $this->context->language->id);
                    $template_vars = [];
                    foreach ($child_categories as $cCat) {
                        $_category = new Category($cCat['id_category'], $lang_id);
                        $products = $_category->getProducts($lang_id, 1, 1000, null, null, false, true, false, 1, false);

                        $order = $this->getOrder($id_bundlefilter, $_category->id);

                        $products = $this->sortArrayByArray($products, $order);
                        $template_manufacturers = [];
                        foreach ($products as $key => $product) {
                            $manufacturer = new Manufacturer($product['id_manufacturer']);
                            $products[$key]['manufacturer_info'] = array(
                                'name' => $manufacturer->name,
                                'id' => $manufacturer->id
                            );
                            if (Validate::isLoadedObject($manufacturer))
                                $template_manufacturers[$manufacturer->id] = $manufacturer->name;
                        }
                        $combination_groups = $this->setCombination_groups($products, $id_bundlefilter, $_category->id, $lang_id);
                        $return_array = array(
                            'combination_groups' => $combination_groups,
                            'products' => $products,
                            'ajax_url_id_' . $this->__nameO => $ajax_url,
                            'catID' => $_category->id,
                            'catName' => $_category->name,
                            'order' => implode(',', $order),
                            'manufacturers' => $template_manufacturers,
                        );
                        $template_vars[$cCat['id_category']] = $return_array;

                    }
                    $this->context->smarty->assign(array(
                        'data' => $template_vars,
                        'childs' => true
                    ));
                } else {

                    $products = $category->getProducts($lang_id, 1, 2, null, null, false, true, false, 1, false);
                    $combination_groups = $this->setCombination_groups($products, $id_bundlefilter, $category->id, $lang_id);
                    $order = $this->getOrder($id_bundlefilter, $category->id);

                    $products = $this->sortArrayByArray($products, $order);


                    $template_manufacturers = [];
                    foreach ($products as $key => $product) {
                        $manufacturer = new Manufacturer($product['id_manufacturer']);
                        $products[$key]['manufacturer_info'] = array(
                            'name' => $manufacturer->name,
                            'id' => $manufacturer->id
                        );
                        if (Validate::isLoadedObject($manufacturer))
                            $template_manufacturers[$manufacturer->id] = $manufacturer->name;
                    }
                    //prepare groups
//                $extra_groups = [];
////                if (isset($product['attrgroups']['combinations'])) {
////                    foreach ($product['attrgroups']['combinations'] as $key2 => &$comb) {
////                        $comb['total_price_wreduc'] = Product::getPriceStatic((int)$product['id_product'], true, $key2, 6, null, false);
////                        $comb['total_price_nreduc'] = Product::getPriceStatic((int)$product['id_product'], true, $key2, 6, null, false, false);
////                        $data_custom = $this->getAttributeGroupCustomFields($key2);
////                        $comb['name'] = $data_custom['coname'];
////                        $comb['qAmountLabel'] = explode(';', $data_custom['qAmountLabel']);
////                        $comb['valueperitem_w'] = $comb['qAmountLabel'][0] != 0 ? $comb['total_price_wreduc'] / $comb['qAmountLabel'][0] : 0;
////                        $comb['valueperitem_n'] = $comb['qAmountLabel'][0] != 0 ? $comb['total_price_nreduc'] / $comb['qAmountLabel'][0] : 0;
////                        $images = $product['attrgroups']['combinationImages'][$key2];
////                        $comb['image'] = $images[0];
////                    }
////                }
//                $category->qAmountLabel = "Paletē";
//                if(isset($category->qAmountLabel) && $category->qAmountLabel != null){
//                    $extra_groups['qAmountLabel']=$category->qAmountLabel;
//                }


                    $this->context->smarty->assign(array(
                        'combination_groups' => $combination_groups,
                        'products' => $products,
                        'ajax_url_id_' . $this->__nameO => $ajax_url,
                        'catID' => $category->id,
                        'childs' => false,
                        'order' => implode(',', $order),
                        'manufacturers' => $template_manufacturers
                    ));
                }


                $destTpl = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'bundlepicker/views/templates/front/ajax_descriptions.tpl');


                $return['a'] = $destTpl;
                die(Tools::jsonEncode($return));
            } elseif (Tools::getIsset('saveCategoryinfo')) {
                $id_category = Tools::getValue('catID');
                $id_products = Tools::getValue('id_products');
                $id_bundlefilter = Tools::getValue('id_bundlefilter');
                $selectedAttributes = Tools::getValue('attribute');


                $order = Tools::getValue('order');
//                $order =explode(',',$order);
//                $order =implode(';',$order);
                $id_filter_products = 0;
                $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
                  WHERE id_bundlefilter = ' . $id_bundlefilter . ' and id_category =' . $id_category;
                if ($row = Db::getInstance()->getRow($sql)) {
                    $id_filter_products = (int)$row['id'];
                    if ($id_products)
                        $id_products = implode(';', $id_products);
                    else
                        $id_products = '';
                    Db::getInstance()->update('filter_products', array(

                        'id_products' => pSQL($id_products),
                        'order' => $order
                    ),
                        'id_bundlefilter = ' . $id_bundlefilter . ' and 
                        id_category = ' . $id_category . ''
                    );
                } else if ($id_products) {
                    $id_products = implode(';', $id_products);
                    $row = Db::getInstance()->insert('filter_products', array(
                        'id_bundlefilter' => (int)$id_bundlefilter,
                        'id_category' => (int)$id_category,
                        'id_products' => pSQL($id_products),
                        'order' => $order
                    ));
                    $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
                        WHERE id_bundlefilter = ' . $id_bundlefilter . ' and id_category =' . $id_category;
                    $row = Db::getInstance()->getRow($sql);
                    $id_filter_products = (int)$row['id'];
                } else {
                    //create empty filter_product
                    $row = Db::getInstance()->insert('filter_products', array(
                        'id_bundlefilter' => (int)$id_bundlefilter,
                        'id_category' => (int)$id_category,
                        'id_products' => '',
                        'order' => $order
                    ));
                    $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
                        WHERE id_bundlefilter = ' . $id_bundlefilter . ' and id_category =' . $id_category;
                    $row = Db::getInstance()->getRow($sql);
                    $id_filter_products = (int)$row['id'];
                }
                //saving combination info
                $from = Tools::getValue('from');
                $to = Tools::getValue('to');
                $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products_filters WHERE id_filter_products = ' . $id_filter_products;
                if ($row = Db::getInstance()->getRow($sql)) {
                    foreach ($from as $id => $val) {
                        $_from = $from[$id];
                        $_to = $to[$id];
                        $string = isset($selectedAttributes[$id]) ? implode(';', $selectedAttributes[$id]) : '';

                        $row = Db::getInstance()->update('filter_products_filters', array(
                            'id_filter_products' => (int)$id_filter_products,
                            'id_group' => $id,
                            'from' => $_from,
                            'to' => $_to,
                            'attributes' => $string
                        ),
                            'id_filter_products = ' . $id_filter_products . ' and 
                        id_group = ' . $id . '');
                    }
                } else {
                    if ($from)
                        foreach ($from as $id => $val) {
                            $_from = $from[$id];
                            $_to = $to[$id];
                            $string = isset($selectedAttributes[$id]) ? implode(';', $selectedAttributes[$id]) : '';
                            $row = Db::getInstance()->insert('filter_products_filters', array(
                                'id_filter_products' => (int)$id_filter_products,
                                'id_group' => $id,
                                'from' => $_from,
                                'to' => $_to,
                                'attributes' => $string
                            ));
                        }
//
                }
                $return['result'] = 'success';
                die(Tools::jsonEncode($return));


            }
            parent::postProcess();
        }
    }

    public function sortArrayByArray(array $array, array $orderArray)
    {
        $ordered = array();

        foreach ($orderArray as $key) {
            foreach ($array as $pkey => $product) {
                if ($product['id_product'] == $key) {
                    $ordered[] = $product;
                    unset($array[$pkey]);
                }
            }
        }
        return $ordered + $array;
    }

    private function getOrder($id_bundlefilter, $id_category)
    {
        $order = "";
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
                        WHERE id_bundlefilter = ' . $id_bundlefilter . ' and id_category =' . $id_category;
        if ($row = Db::getInstance()->getRow($sql))
            $order = $row['order'];
        return explode(',', $order);
    }

    private function setCombination_groups(&$products, $id_bundlefilter, $id_category, $lang_id)
    {
        $combination_groups = [];
        $attributes = [];
        foreach ($products as $key => &$product) {
            if ($this->checkifProductselected($id_bundlefilter, $id_category, $product['id_product'])) {
                $products[$key]['checked'] = true;
            };
            $product['attrgroups'] = $this->assignAttributesGroups($product['id_product']);
            //$products[$key]['attributes'] = $this->assignAttributesCombinations($product['id_product']);
            if (isset($product['attrgroups']['groups'])) {

                foreach ($product['attrgroups']['groups'] as $key2 => $group) {
                    if (!isset($combination_groups[$key2])) {
                        $combination_groups[$key2]['name'] = $group['name'];
//                        $combination_groups[$key2]['value']['all_attributes'] = [];
                        if ($values = $this->getFiltersProductFiltersValues($id_bundlefilter, $id_category, $key2)) {
                            $combination_groups[$key2]['value']['from'] = $values['from'];
                            $combination_groups[$key2]['value']['to'] = $values['to'];
                            if (sizeof($values['attributes']) > 0)
                                $combination_groups[$key2]['value']['selected_attr'] = explode(';', $values['attributes']);
                            else
                                $combination_groups[$key2]['value']['selected_attr'] = array();

                        } else {
                            $combination_groups[$key2]['value']['from'] = 0;
                            $combination_groups[$key2]['value']['to'] = 0;
                            $combination_groups[$key2]['value']['selected_attr'] = array();
                        };


                    }

                    foreach ($group['attributes'] as $aID => $attribute) {
                        $attributes[$key2][$aID] = $attribute;
                    }

                }

            }

        }

        foreach ($attributes as $id1 => $attribute) {

            $combination_groups[$id1]['value']['all_attributes'] = $attribute;
        }

        //Get selected attribs;
        //  $filter_products = new Filter_products()
        return $combination_groups;
    }

    private function getFiltersProductFiltersValues($id_bundlefilter, $id_category, $id_group)
    {

        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
            WHERE id_bundlefilter = ' . $id_bundlefilter . ' and id_category =' . $id_category;
        if ($row = Db::getInstance()->getRow($sql)) {
            $id_filters_product = $row['id'];
            $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products_filters WHERE id_filter_products = ' . $id_filters_product . ' and id_group = ' . $id_group;
            if ($row = Db::getInstance()->getRow($sql)) {
                return array(
                    'from' => $row['from'],
                    'to' => $row['to'],
                    'attributes' => $row['attributes']
                );
            }


        }
        return false;
    }

    private function checkifProductselected($id_bundlefilter, $category, $product)
    {
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'filter_products
    WHERE id_bundlefilter = ' . $id_bundlefilter . ' and id_category =' . $category;
        if ($row = Db::getInstance()->getRow($sql)) {
            $products = explode(';', $row['id_products']);
            foreach ($products as $id_product) {
                if ($id_product == $product) return true;
            }
        }
        return false;
    }

    private function _postValidation()
    {
        $cookie = new Cookie('psAdmin', '', (int)Configuration::get('PS_COOKIE_LIFETIME_BO'));
        $employee = new Employee((int)$cookie->id_employee);

        if (Validate::isLoadedObject($employee) && $employee->checkPassword((int)$cookie->id_employee, $cookie->passwd)
            && (!isset($cookie->remote_addr) || $cookie->remote_addr == ip2long(Tools::getRemoteAddr()) || !Configuration::get('PS_COOKIE_CHECKIP'))
        ) {
            return true;
        } else
            die('User is not logged in');


    }

    private function assignAttributesCombinations($prod_id)
    {
        $attributes_combinations = Product::getAttributesInformationsByProduct($prod_id);
        if (is_array($attributes_combinations) && count($attributes_combinations)) {
            foreach ($attributes_combinations as &$ac) {
                foreach ($ac as &$val) {
                    $val = str_replace(Configuration::get('PS_ATTRIBUTE_ANCHOR_SEPARATOR'), '_', Tools::link_rewrite(str_replace(array(',', '.'), '-', $val)));
                }
            }
        } else {
            $attributes_combinations = array();
        }
        return array(
            'attributesCombinations' => $attributes_combinations,
            'attribute_anchor_separator' => Configuration::get('PS_ATTRIBUTE_ANCHOR_SEPARATOR')
        );
    }

    /**
     * Assign template vars related to attribute groups and colors
     */

    private function assignAttributesGroups($prod_id)
    {
        $colors = array();
        $groups = array();
        $product = new Product($prod_id);
        $context = Context::getContext();
        $attributes_groups = $product->getAttributesGroups($context->language->id);
        if (is_array($attributes_groups) && $attributes_groups) {
            $combination_images = $product->getCombinationImages($context->language->id);
            $combination_prices_set = array();
            foreach ($attributes_groups as $k => $row) {
                if (isset($row['is_color_group']) && $row['is_color_group'] && (isset($row['attribute_color']) && $row['attribute_color']) || (file_exists(_PS_COL_IMG_DIR_ . $row['id_attribute'] . '.jpg'))) {
                    $colors[$row['id_attribute']]['value'] = $row['attribute_color'];
                    $colors[$row['id_attribute']]['name'] = $row['attribute_name'];
                    if (!isset($colors[$row['id_attribute']]['attributes_quantity'])) {
                        $colors[$row['id_attribute']]['attributes_quantity'] = 0;
                    }
                    $colors[$row['id_attribute']]['attributes_quantity'] += (int)$row['quantity'];
                }
                if (!isset($groups[$row['id_attribute_group']])) {
                    $groups[$row['id_attribute_group']] = array(
                        'group_name' => $row['group_name'],
                        'name' => $row['public_group_name'],
                        'group_type' => $row['group_type'],
                        'default' => -1,
                    );
                }
                $groups[$row['id_attribute_group']]['attributes'][$row['id_attribute']] = $row['attribute_name'];
                if ($row['default_on'] && $groups[$row['id_attribute_group']]['default'] == -1) {
                    $groups[$row['id_attribute_group']]['default'] = (int)$row['id_attribute'];
                }
                if (!isset($groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']])) {
                    $groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] = 0;
                }
                $groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] += (int)$row['quantity'];
                $combinations[$row['id_product_attribute']]['attributes_values'][$row['id_attribute_group']] = $row['attribute_name'];
                $combinations[$row['id_product_attribute']]['attributes'][] = (int)$row['id_attribute'];
                $combinations[$row['id_product_attribute']]['price'] = (float)Tools::convertPriceFull($row['price'], null, Context::getContext()->currency, false);
                if (!isset($combination_prices_set[(int)$row['id_product_attribute']])) {
                    Product::getPriceStatic((int)$product->id, false, $row['id_product_attribute'], 6, null, false, true, 1, false, null, null, null, $combination_specific_price);
                    $combination_prices_set[(int)$row['id_product_attribute']] = true;
                    $combinations[$row['id_product_attribute']]['specific_price'] = $combination_specific_price;
                }
                $combinations[$row['id_product_attribute']]['ecotax'] = (float)$row['ecotax'];
                $combinations[$row['id_product_attribute']]['weight'] = (float)$row['weight'];
                $combinations[$row['id_product_attribute']]['quantity'] = (int)$row['quantity'];
                $combinations[$row['id_product_attribute']]['reference'] = $row['reference'];
                $combinations[$row['id_product_attribute']]['unit_impact'] = Tools::convertPriceFull($row['unit_price_impact'], null, Context::getContext()->currency, false);
                $combinations[$row['id_product_attribute']]['minimal_quantity'] = $row['minimal_quantity'];
                if ($row['available_date'] != '0000-00-00' && Validate::isDate($row['available_date'])) {
                    $combinations[$row['id_product_attribute']]['available_date'] = $row['available_date'];
                    $combinations[$row['id_product_attribute']]['date_formatted'] = Tools::displayDate($row['available_date']);
                } else {
                    $combinations[$row['id_product_attribute']]['available_date'] = $combinations[$row['id_product_attribute']]['date_formatted'] = '';
                }
                if (!isset($combination_images[$row['id_product_attribute']][0]['id_image'])) {
                    $combinations[$row['id_product_attribute']]['id_image'] = -1;
                } else {
                    $combinations[$row['id_product_attribute']]['id_image'] = $id_image = (int)$combination_images[$row['id_product_attribute']][0]['id_image'];
                    if ($row['default_on']) {
                        if ($id_image > 0) {
                            if (isset($context->smarty->tpl_vars['images']->value)) {
                                $product_images = $context->smarty->tpl_vars['images']->value;
                            }
                            if (isset($product_images) && is_array($product_images) && isset($product_images[$id_image])) {
                                $product_images[$id_image]['cover'] = 1;
                                $context->smarty->assign('mainImage', $product_images[$id_image]);
                                if (count($product_images)) {
                                    $context->smarty->assign('images', $product_images);
                                }
                            }
                            if (isset($context->smarty->tpl_vars['cover']->value)) {
                                $cover = $context->smarty->tpl_vars['cover']->value;
                            }
                            if (isset($cover) && is_array($cover) && isset($product_images) && is_array($product_images)) {
                                $product_images[$cover['id_image']]['cover'] = 0;
                                if (isset($product_images[$id_image])) {
                                    $cover = $product_images[$id_image];
                                }
                                $cover['id_image'] = (Configuration::get('PS_LEGACY_IMAGES') ? ($product->id . '-' . $id_image) : (int)$id_image);
                                $cover['id_image_only'] = (int)$id_image;
                                $context->smarty->assign('cover', $cover);
                            }
                        }
                    }
                }
            }
            if (!Product::isAvailableWhenOutOfStock($product->out_of_stock) && Configuration::get('PS_DISP_UNAVAILABLE_ATTR') == 0) {
                foreach ($groups as &$group) {
                    foreach ($group['attributes_quantity'] as $key => &$quantity) {
                        if ($quantity <= 0) {
                            unset($group['attributes'][$key]);
                        }
                    }
                }
                foreach ($colors as $key => $color) {
                    if ($color['attributes_quantity'] <= 0) {
                        unset($colors[$key]);
                    }
                }
            }
            foreach ($combinations as $id_product_attribute => $comb) {
                $attribute_list = '';
                foreach ($comb['attributes'] as $id_attribute) {
                    $attribute_list .= '\'' . (int)$id_attribute . '\',';
                }
                $attribute_list = rtrim($attribute_list, ',');
                $combinations[$id_product_attribute]['list'] = $attribute_list;
            }
            return array(
                'groups' => $groups,
                'colors' => (count($colors)) ? $colors : false,
                'combinations' => $combinations,
                'combinationImages' => $combination_images
            );
        }
    }

    private function getAttributeGroupCustomFields($id_product_attribute)
    {
        $combination = new Combination($id_product_attribute);
        $data['qAmountLabel'] = $combination->qAmountLabel;
        $data['coname'] = $combination->coname;
        return $data;
    }
}