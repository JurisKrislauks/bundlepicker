<?php

class AdminBundlePickerController extends ModuleAdminController
{
    public $imageType = 'jpg';
    protected $_bundlepicker = null;

    public function __construct()
    {
        $this->__nameO = 'bundlepicker';
        $this->className = $this->__nameO . 'Mod';
        $this->table = $this->__nameO;
        $this->meta_title = $this->l('Category');
        $this->deleted = false;
        $this->explicitSelect = true;
        $this->context = Context::getContext();
        $this->lang = true;
        $this->bootstrap = true;
        $this->meta_title = array($this->l('Category'));


        if (Shop::isFeatureActive()) {
            Shop::addTableAssociation($this->table, array('type' => 'shop'));
        }

        $this->position_identifier = 'id_' . $this->__nameO;

        $this->addRowAction('edit');
        $this->addRowAction('view');
        $this->addRowAction('delete');

        $this->fieldImageSettings = array(
            'name' => 'logo',
            'dir' => $this->__nameO
        );
        $this->fields_list = array(
            'id_' . $this->__nameO => array(
                'title' => $this->l('ID'),
                'type' => 'int',
                'class' => 'fixed-width-xs',
                'orderby' => false
            ),
            'image' => array(
                'title' => $this->l('Logo'),
                'image' => $this->__nameO,
                'orderby' => false,
                'search' => false,
                'align' => 'center',

            ),
            'title' => array(
                'title' => $this->l('Title'),
                'align' => 'center',
                'orderby' => false
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'width' => 'auto',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false,
                'class' => 'fixed-width-xs',
            ),


        );

        parent::__construct();


    }

    public function initToolBarTitle()
    {
        $this->toolbar_title[] = $this->l('Administration');
        $this->toolbar_title[] = $this->l('Categories');
    }
    /* ------------------------------------------------------------- */
    /*  INIT PAGE HEADER TOOLBAR
    /* ------------------------------------------------------------- */
    public function initPageHeaderToolbar()
    {

        if (empty($this->display)) {
            $this->page_header_toolbar_btn = array(
                'new' => array(
                    'href' => self::$currentIndex . '&add' . $this->__nameO . '&token=' . $this->token,
                    'desc' => $this->l('Add New Category', null, null, false),
                    'icon' => 'process-icon-new'
                )
            );
        }

        parent::initPageHeaderToolbar();
    }


    public function renderForm()
    {

        if (!($object = $this->loadObject(true))) {
            return;
        }

        $image = _PS_IMG_DIR_ . $this->__nameO . '/' . $object->id . '.jpg';
        $image_url = ImageManager::thumbnail($image, $this->table . '_' . (int)$object->id . '.' . $this->imageType, 350,
            $this->imageType, true, true);
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;

        // Init Fields form array
        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Menu'),
                'icon' => 'icon-cogs'
            ),
            // Inputs
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'name' => 'title',
                    'desc' => $this->l('Must be less than 125 characters.'),
                    'required' => true,
                    'lang' => false
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    'desc' => $this->l('Must be less than 250 characters.'),
                    'required' => true,
                    'lang' => true
                ),
//                array(
//                    'type' => 'textarea',
//                    'label' => $this->l('Description'),
//                    'name' => 'description',
//
//                    'autoload_rte' => 'rte', //Enable TinyMCE editor for description
//                    'required' => false,
//                    'lang' => true
//                ),

                array(
                    'type' => 'file',
                    'label' => $this->l('Logo'),
                    'name' => 'logo',
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'display_image' => true,
                    'col' => 6,
                    'hint' => $this->l('Upload a Pretty image logo from your computer.')
                ),

                array(
                    'type'  => 'hidden',
                    'name'  => 'id_parent',
                    'value' => Tools::getValue('id_parent')

                ),

            ),
            // Submit Button
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'saveBundlePickerroot'
            )
        );
//        $id_parent = 0;
//        $id_parent = Tools::getValue('id_parent');
//
//        $depth = bundlepickerMod::getBundleDepth($id_parent);
//        if($depth>0){
//            $this->fields_form['input'][]=array(
//
//                    'type' => 'categories',
//                    'label' => $this->l('Categories 2'),
//                    'name' => 'cat_ids',
//                    'tree' => array(
//                        'id' => 'id_root_bundlepicker2',
//                        'use_checkbox' => true,
//                        //'selected_categories' => explode(';', $object->cat_id),
//                    ),
//
//            );
//        }else{
//            $this->fields_form['input'][]=array(
//
//                'type' => 'categories',
//                'label' => $this->l('Category'),
//                'name' => 'cat_id',
//                'tree' => array(
//                    'id' => 'id_root_bundlepicker',
//                    'use_checkbox' => false,
//                    'selected_categories' => explode(';', $object->cat_id),
//                ),
//
//            );
//        }
        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
            );
        }

        return parent::renderForm();
    }

    public function renderList()
    {
        if (isset($this->_filter) && trim($this->_filter) == '') {
            $this->_filter = $this->original_filter;
        }

        $this->addRowAction('view');
        $this->addRowAction('add');
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        return parent::renderList();
    }

    public function init()
    {
        parent::init();
        $this->checkDepth();
        // context->shop is set in the init() function, so we move the _bundlepicker instanciation after that
        if (($id_bundlepicker = Tools::getvalue('id_bundlepicker')) && $this->action != 'select_delete') {
            $this->_bundlepicker = new bundlepickerMod($id_bundlepicker);
        }

        $id_parent = 0;
        if (Tools::isSubmit('id_bundlepicker')) {
            $id_parent = $this->_bundlepicker->id;
        }

        $this->_select = 'a.`id_parent`';
        $this->original_filter = $this->_filter .= ' AND `id_parent` = ' . (int)$id_parent . ' ';
        $this->_use_found_rows = false;

        $this->_join .= ' LEFT JOIN `' . _DB_PREFIX_ . 'bundlepicker_shop` sa ON (a.`id_bundlepicker` = sa.`id_bundlepicker` AND sa.id_shop = a.id_shop_default) ';

    }
    public function checkDepth(){
        $id_object = Tools::getValue('id_' . $this->__nameO);
        $depth = bundlepickerMod::getBundleDepth($id_object);
        if($depth >1 && !Tools::getIsset('updatebundlepicker') && !Tools::getIsset('submitAddbundlepicker')){
            $link = $this->context->link->getAdminLink('AdminBundleFilter').'&id_bundle='.$id_object;
            Tools::redirectAdmin($link);
        }
    }
    public function renderView()
    {

       $this->checkDepth();
        $this->initToolbar();
        return $this->renderList();

    }

    public function initToolbar()
    {
        if (empty($this->display)) {
            $this->toolbar_btn['new'] = array(
                'href' => self::$currentIndex . '&add' . $this->table . '&token=' . $this->token,
                'desc' => $this->l('Add New')
            );

            if ($this->can_import) {
                $this->toolbar_btn['import'] = array(
                    'href' => $this->context->link->getAdminLink('AdminImport', true) . '&import_type=categories',
                    'desc' => $this->l('Import')
                );
            }
        }

        if (Tools::getValue('id_bundlepicker') && !Tools::isSubmit('updatecategory')) {
            $this->toolbar_btn['edit'] = array(
                'href' => self::$currentIndex . '&update' . $this->table . '&id_bundlepicker=' . (int)Tools::getValue('id_bundlepicker') . '&token=' . $this->token,
                'desc' => $this->l('Edit')
            );
        }

        if ($this->display == 'view') {
            $this->toolbar_btn['new'] = array(
                'href' => self::$currentIndex . '&add' . $this->table . '&id_parent=' . (int)Tools::getValue('id_bundlepicker') . '&token=' . $this->token,
                'desc' => $this->l('Add New')
            );
        }
        parent::initToolbar();

        // after adding a category
        if (empty($this->display)) {
            $id_bundlepicker = (Tools::isSubmit('id_bundlepicker')) ? '&id_parent=' . (int)Tools::getValue('id_bundlepicker') : '';
            $this->toolbar_btn['new'] = array(
                'href' => self::$currentIndex . '&add' . $this->table . '&token=' . $this->token . $id_bundlepicker,
                'desc' => $this->l('Add New')
            );

            if (Tools::isSubmit('id_bundlepicker')) {
                $back = Tools::safeOutput(Tools::getValue('back', ''));
                if (empty($back)) {
                    $back = self::$currentIndex . '&token=' . $this->token;
                }
                $this->toolbar_btn['back'] = array(
                    'href' => $back,
                    'desc' => $this->l('Back to list')
                );
            }
        }
    }

    public function processAdd()
    {
//        $id_category = (int)Tools::getValue('id_category');
        $id_parent = (int)Tools::getValue('id_parent');

        // if true, we are in a root category creation
        if (!$id_parent) {

            $_POST['id_parent'] = $id_parent = 0;
        }

        $object = parent::processAdd();


        return $object;
    }

}