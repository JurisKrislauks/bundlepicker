jQuery(document).ready(function($) {
    // prepare the html element that will display the description
    var producticonPopup = $('.product-icons-popup');
    var screenHeight = $(window).height();
    console.log(screenHeight);
    // click on the icon
    $('.product-icon-link').click(function() {
        var producticonId = $(this).attr('data-id');
        $('.product-icons-popup-content').empty();
        displayDescription(producticonId);
        return false;
    });
    $('.product-icons-popup-close').click(function() {
        closePopup()
    });
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // 27 is ESC
            closePopup()
        }
    });

    function closePopup() {
        producticonPopup.removeClass('active');
        producticonPopup.removeClass('center-vertical');
        $('body').removeClass('noscroll');
    }

    function centerPopup(popupHeight, windowHeight) {
        if (popupHeight < windowHeight) {
            producticonPopup.addClass('center-vertical');
        } else {
            producticonPopup.removeClass('center-vertical');
        }
    }

    function displayDescription(producticonId) {
        ajaxQuery = $.ajax({
            type: 'POST',
            url: baseDir + 'modules/bundlepicker/bundlepicker-ajax.php',
            data: {
                stproject_id: producticonId
            },
            dataType: 'json',
            cache: false, // @todos ee a way to use cache and to add a timestamps parameter to refresh cache each 10 minutes for example
            success: function(result) {
                console.log('ajax data loaded');
                var popupContent = $('.product-icons-popup-content');
                var popupWrapper = $('.product-icons-popup-wrapper');
                popupContent.html(result['content']);
                var popupH = popupWrapper.outerHeight();
                // check if there's any images
                imageCount = popupContent.find('img').length;
                if (imageCount) {
                    var imagesLoaded = 0;
                    $('.product-icons-popup-content img').on('load', function() {
                        popupH += $(this).outerHeight();
                        imagesLoaded++;
                        if (imagesLoaded == imageCount) {
                            console.log('content h is: ' + popupH);
                            centerPopup(popupH, screenHeight);
                            $('body').addClass('noscroll');
                            producticonPopup.addClass('active');
                        }
                    });
                } else {
                    console.log('content h is: ' + popupH);
                    centerPopup(popupH, screenHeight);
                    $('body').addClass('noscroll');
                    producticonPopup.addClass('active');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if (textStatus !== 'abort') {
                    error = "Error";
                    if (!!$.prototype.fancybox) $.fancybox.open([{
                        type: 'inline',
                        autoScale: true,
                        minHeight: 30,
                        content: '<p class="fancybox-error">' + errorThrown + '</p>'
                    }], {
                        padding: 0
                    });
                    else alert(error);
                }
                $('#opc_new_account-overlay, #opc_delivery_methods-overlay, #opc_payment_methods-overlay').fadeIn('slow')
            }
        });
    }
});