var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    compass = require('gulp-compass'),
    browserSync = require('browser-sync').create(),
    uglify = require('gulp-uglify'),
    cache = require('gulp-cached'),
    concat = require('gulp-concat'),
    pump = require('pump'),
    gutil = require( 'gulp-util' ),
    changed = require('gulp-changed'),
    autoprefixer = require('gulp-autoprefixer');



var paths = {
    scripts: ['./views/javascript/**/**/*.js'],
    sass: './views/sass/**/**/*.scss',
    tpl: './views/**/**/*.tpl',
    css: './views/css/**/**/*.css'
};



gulp.task('serve', ['sass', 'js'], function() {
    //browserSync.init({
    //    proxy: "localhost"
    //});
    gulp.watch(paths.sass, ['sass']);

    // use this line to watch only css files
    //gulp.watch(paths.css).on('change', browserSync.reload); 
    gulp.watch(paths.scripts, ['js-watch']);
    //gulp.watch(paths.tpl).on('change', browserSync.reload);
});


gulp.task('js', function(cb) {
    pump([
            gulp.src(paths.scripts),
            sourcemaps.init(),
            cache('linting'),
            //concat('all.min.js'),
            uglify(),
            sourcemaps.write(),
            gulp.dest('./views/js')
        ],
        cb
    );
});


gulp.task('js-watch', ['js'], function(done) {
    browserSync.reload();
    done();
});


gulp.task('sass', function() {
    //gulp.src('./sass/{,*/}*.{scss,sass}')
    gulp.src('./views/sass/**/*.{scss,sass}')
        .pipe(sourcemaps.init())
        .pipe(compass({
            config_file: './config.rb',
            css: './views/css',
            sass: './views/sass'
        }))
        .pipe(sass({
            errLogToConsole: true,
            sourcemap: true
        }))
        .pipe(sourcemaps.write('./', {
            includeContent: true,
            sourceRoot: './views/sass/'
        }))
        .pipe(autoprefixer())
        .pipe(gulp.dest('./views/css'))
        //.pipe(browserSync.stream());
});





gulp.task('default', ['serve']);