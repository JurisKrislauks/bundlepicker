<?php

if (!defined('_PS_VERSION_'))
    exit;

include_once(_PS_MODULE_DIR_ . 'bundlepicker/model/bundlepickerMod.php');
include_once(_PS_MODULE_DIR_ . 'bundlepicker/model/bundlefilterMod.php');

class BundlePicker extends Module
{
    // DB file
    const INSTALL_SQL_FILE = 'install.sql';

    public function __construct()
    {
        $this->name = 'bundlepicker';

        $this->tab = 'front_office_features';
        $this->version = '0.0.1';
        $this->author = 'Stratcom Džuris';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');
        // $this->dependencies = array('blockcart');

        //Variable used for sql table actions
        $this->table_name = "bundlepicker";
        $this->filter_table_name = "bundlefilter";
        $this->filterProducts_table = "filter_products";
        $this->bootstrap = true;
        $this->displayName = $this->l('Bundle picker module');
        $this->description = $this->l('Adds customizable bundle picker');
        parent::__construct();



        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        $this->_html = "";

//        $this->my_variable = Configuration::get('MY_VARIABLE'); // mainīgo definēšana


    }

    /**
     * install
     */
    public function install()
    {
        // Create DB tables - uncomment below to use the install.sql for database manipulation


        include dirname(__FILE__) . '/sql/install.php';

        if (!parent::install() ||

            !$this->registerHook('displayHeader') ||

            !$this->registerHook('displayBackOfficeHeader')

            || !$this->_createTab()
            || !$this->_createConfigs()
        )
            return false;
        //Image directory making
        if (!file_exists(_PS_IMG_DIR_ . 'bundlepicker/'))
            if (!mkdir(_PS_IMG_DIR_ . 'bundlepicker')) return false;

//        //Visual composer integration
//        if (!$this->installTpls()) return false;
//        if (Module::isInstalled('jscomposer') && Module::isEnabled('jscomposer')) {
//            $attribs = array(
//                'bundlepicker' => array(
//                    'controller' => 'AdminBundlePicker',
//                    'identifier' => 'id_bundlepicker',
//                    'shortname' => 'bundlepicker',
//                    'field' => 'description',
//
//                ));
//            if (!JsComposer::AddVcExternalControllers($attribs)) return false;
//
//        }

        return true;
    }

    public function installTpls()
    {

        // END TINYMCE EDITOR
        $dir = _PS_MODULE_DIR_ . 'bundlepicker/override/controllers/admin/templates/';
        $dstdir = _PS_OVERRIDE_DIR_ . 'controllers/admin/templates/';
        if (!is_dir($dstdir))
            @mkdir($dstdir, 0777);
        if (is_dir($dstdir)) {
            $folder = opendir($dir);
            while (false !== ($folders = readdir($folder))) {
                $fn = $folders;
                if ($fn !== '.' && $fn !== '..') {
                    if (is_dir($dir . $fn)) {
                        $folder2 = opendir($dir . $fn);
                        while (($file = readdir($folder2)) !== false) {
                            if ($file !== '.' && $file !== '..') {
                                @mkdir($dstdir . $fn, 0777);
                                $dstfile = $dstdir . $fn . '/' . $file;
                                Tools::copy($dir . $fn . '/' . $file, $dstfile);
                            }
                        }
                    } else {
                        $dstfile = $dstdir . $fn;
                        Tools::copy($dir . $fn, $dstfile);
                    }
                }
            }
        }
        return true;
    }
    /* ------------------------------------------------------------- */
    /*  CREATE CONFIGS
    /* ------------------------------------------------------------- */
    private function _createConfigs()
    {

        $response = Configuration::updateValue('BUNDLEPICKER_AJAX', 1);


        return $response;
    }

    /* ------------------------------------------------------------- */
    /*  DELETE CONFIGS
    /* ------------------------------------------------------------- */
    private function _deleteConfigs()
    {

        $response = Configuration::deleteByName('BUNDLEPICKER_AJAX');


        return $response;
    }
    /* ------------------------------------------------------------- */
    /*  CREATE THE TAB MENU
    /* ------------------------------------------------------------- */
    private function _createTab()
    {
        $response = true;

        // First check for parent tab
        $parentTabID = Tab::getIdFromClassName('StratcomModules');

        if ($parentTabID) {
            $parentTab = new Tab($parentTabID);
        } else {
            $parentTab = new Tab();
            $parentTab->active = 1;
            $parentTab->name = array();
            $parentTab->class_name = "StratcomModules";
            foreach (Language::getLanguages() as $lang) {
                $parentTab->name[$lang['id_lang']] = "Stratcom modules";
            }
            $parentTab->id_parent = 0;
            $parentTab->module = $this->name;
            $response &= $parentTab->add();
        }

        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = "AdminBundlePicker";
        $tab->name = array();
        foreach (Language::getLanguages() as $lang) {
            $tab->name[$lang['id_lang']] = "Bundle Configurator";
        }
        $tab->id_parent = $parentTab->id;
        $tab->module = $this->name;
        $response &= $tab->add();


        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = "AdminBundleFilter";
        $tab->name = array();
        foreach (Language::getLanguages() as $lang) {
            $tab->name[$lang['id_lang']] = "Filter Configurator";
        }
        $tab->id_parent = $parentTab->id;
        $tab->module = $this->name;
        $tab->active = 0;
        $response &= $tab->add();

        return $response;
    }


    /* ------------------------------------------------------------- */
    /*  DELETE THE TAB MENU
    /* ------------------------------------------------------------- */
    private function _deleteTab()
    {


        do {
            $id_tab = Tab::getIdFromClassName('AdminBundlePicker');
            $tab = new Tab($id_tab);
            $tab->delete();
            $id_tab2 = Tab::getIdFromClassName('AdminBundleFilter');
            $tab = new Tab($id_tab2);
            $tab->delete();
        } while ($id_tab != false && $id_tab2 != false);
        $parentTabID = Tab::getIdFromClassName('StratcomModules');


        // Get the number of tabs inside our parent tab
        // If there is no tabs, remove the parent
        $tabCount = Tab::getNbTabs($parentTabID);
        if ($tabCount == 0) {
            $parentTab = new Tab($parentTabID);
            $parentTab->delete();
        }

        return true;
    }


    /**
     * uninstall
     */
    public function uninstall()
    {

         include dirname(__FILE__) . '/sql/uninstall.php';
        if (!parent::uninstall() ||
            !$this->_deleteTab()
        )
            return false;

        return true;
    }

    /**
     * admin page
     */
    public function getContent() // viss, kas ir šeit būs redzams moduļa admina panelī!
    {
        $this->_postProcess();
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(

//                    array(
//                        'type' => 'text',
//                        'label' => $this->l('Maximum depth'),
//                        'name' => 'BLOCK_CATEG_MAX_DEPTH',
//                        'desc' => $this->l('Set the maximum depth of category sublevels displayed in this block (0 = infinite).'),
//                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Ajax icons'),
                        'name' => 'BUNDLEPICKER_AJAX',
                        'desc' => $this->l('Loads content in ajax popup windows'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
//                    array(
//                        'type' => 'radio',
//                        'label' => $this->l('Sort'),
//                        'name' => 'BLOCK_CATEG_SORT',
//                        'values' => array(
//                            array(
//                                'id' => 'name',
//                                'value' => 1,
//                                'label' => $this->l('By name')
//                            ),
//                            array(
//                                'id' => 'position',
//                                'value' => 0,
//                                'label' => $this->l('By position')
//                            ),
//                        )
//                    ),

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitBundlePicker';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));


    }
    public function getConfigFieldsValues()
    {
        return array(
            'BUNDLEPICKER_AJAX' => (int)Tools::getValue('BUNDLEPICKER_AJAX', Configuration::get('BUNDLEPICKER_AJAX')),

        );
    }

    protected function initForm()
    {
        $helper = new HelperForm();

        $helper->module = $this;
        $helper->name_controller = 'clone';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $this->context->controller->_languages;
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->default_form_language = $this->context->controller->default_form_language;
        $helper->allow_employee_form_lang = $this->context->controller->allow_employee_form_lang;
        $helper->toolbar_scroll = true;


        return $helper;
    }


    private function _postProcess()
    {$output = '';
        if (Tools::isSubmit('submitBundlePicker'))
        {
            $ajax = (int)(Tools::getValue('BUNDLEPICKER_AJAX'));

            if (!is_bool((boolean)$ajax))
                $output .= $this->displayError($this->l('Invalid Ajax value'));
            else
            {
                Configuration::updateValue('BUNDLEPICKER_AJAX', (int)$ajax);

                $this->_clearTempCache();

                Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&conf=6');
            }
        }
    }
    private function _clearTempCache()
    {
        $this->_clearCache('ajax_descriptions.tpl');
        $this->_clearCache('display.tpl');
    }

    private function _postValidation()
    {

    }


    /* This function is the core of the config page for your module.
        Include your options here. */
    private function _displayForm()
    {
        return "hey here ;)";
    }


    public function ajaxCall()
    {
        $bundlepicker_id = Tools::getValue('bundlepicker_id');
        $project = new bundlepickerMod($bundlepicker_id, Context::getContext()->language->id);

        if (!$project->active)
            return false;
        if ((bool)Module::isEnabled('jscomposer')) {
            $composer = Module::getInstanceByName('jscomposer');
            $composer->init();
            $project->description = $composer->do_shortcode($project->description);

        }
        if ((bool)Module::isEnabled('smartshortcode')) {
            $smartshortcode = Module::getInstanceByName('smartshortcode');
            $project->description = $smartshortcode->parse($project->description);
        }
        $this->context->smarty->assign(array(
            'producticon_object' => $project,
            'producticon_title' => $project->title,
            'producticon_description' => $project->description


        ));

        $destTpl = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'bundlepicker/views/templates/front/ajax_descriptions.tpl');

        $this->context->smarty->assign($destTpl);
        $vars = array('content' => $destTpl);

        echo Tools::jsonEncode($vars);


    }

    // BACK OFFICE HOOKS

    /**
     * admin <head> Hook
     */
    public function hookDisplayBackOfficeHeader()
    {
        // CSS
        $this->context->controller->addCSS($this->_path . 'views/css/admin.css');
        // JS
         $this->context->controller->addJS($this->_path.'views/js/admin/filters.js');
         if(Tools::getValue('controller') == 'AdminBundleFilter')
         $this->context->controller->addJS($this->_path.'views/js/admin/plugins/jquery-ui/jquery-ui.min.js');
    }





    // FRONT OFFICE HOOKS

    /**
     * <head> Hook
     */
    public function hookDisplayHeader()
    {
        // CSS
        $this->context->controller->addCSS($this->_path . 'views/css/bundlepicker.css');
        // JS
        $this->context->controller->addJS($this->_path . 'views/js/' . $this->name . '.js');
        $this->context->controller->addJS($this->_path . 'bundlepicker.js');
    }

    /**
     * Top of pages hook
     */
    public function hookDisplayTop($params)
    {
        return $this->hookDisplayHome($params);
    }


    /**
     * Home page hook
     */
    public function hookDisplayHome($params) // hooks atbild par satura attēlošanu frontā
    {
        $object = bundlepickerMod::getAllIcons(4);
        $this->context->smarty->assign(array(
            'bundlepicker' => $object,
            'ajax'=>(int)Configuration::get('BUNDLEPICKER_AJAX')
        ));
        return $this->display(__FILE__, 'views/templates/front/display.tpl');

    }

    /**
     * Left Column Hook
     */
    public function hookDisplayRightColumn($params)
    {
        return $this->hookDisplayHome($params);
    }

    /**
     * Right Column Hook
     */
    public function hookDisplayLeftColumn($params)
    {
        return $this->hookDisplayHome($params);
    }

    /**
     * Footer hook
     */
    public function hookDisplayFooter($params)
    {
        return $this->hookDisplayHome($params);
    }


}

?>
