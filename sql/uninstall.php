<?php


$sql = array();
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . $this->table_name.'`;';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . $this->table_name.'_lang`;';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . $this->table_name.'_shop`;';

$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . $this->filter_table_name.'`;';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . $this->filter_table_name.'_lang`;';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . $this->filter_table_name.'_shop`;';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . $this->filterProducts_table.'`;';
$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . $this->filterProducts_table.'_filters`;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
