<?php


$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->table_name . '` (
        `id_bundlepicker` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `title` VARCHAR(64) NOT NULL,
        `active` BOOLEAN DEFAULT TRUE  NOT NULL,

        `id_parent` int(10) unsigned NOT NULL,
        `id_shop_default` int(10) unsigned NOT NULL,
        PRIMARY KEY  (`id_bundlepicker`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';
//        `cat_id` int(10) unsigned NOT NULL,
//$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->table_name.'_assigned` (
//        `id_product` int(10) unsigned NOT NULL,
//        `id_stprojects` int(10) NOT NULL
//
//) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->table_name . '_shop` (
        
        `id_bundlepicker` int(10) unsigned NOT NULL,
        `id_shop` int unsigned NOT NULL DEFAULT 1
       
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

//`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->table_name . '_lang` (

        `id_bundlepicker` int(10) unsigned NOT NULL,
        `id_lang` int unsigned NOT NULL DEFAULT 1,
        `name` varchar(255)  NOT NULL
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';
//,
//`description` TEXT


//FILTERS
$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->filter_table_name . '` (
        `id_bundlefilter` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `title` VARCHAR(64) NOT NULL,
        `active` BOOLEAN DEFAULT TRUE  NOT NULL,
         `cat_id` VARCHAR(256) NOT NULL,
        `id_bundle` int(10) unsigned NOT NULL,
        `id_shop_default` int(10) unsigned NOT NULL,
        PRIMARY KEY  (`id_bundlefilter`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';


$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->filter_table_name . '_shop` (
        
        `id_bundlefilter` int(10) unsigned NOT NULL,
        `id_shop` int unsigned NOT NULL DEFAULT 1
       
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';


$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->filter_table_name . '_lang` (

        `id_bundlefilter` int(10) unsigned NOT NULL,
        `id_lang` int unsigned NOT NULL DEFAULT 1,
        `name` varchar(255)  NOT NULL,
        `description` TEXT
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->filterProducts_table . '` (
         `id` int unsigned not null auto_increment,
        `id_bundlefilter` int(10) unsigned NOT NULL,
        `id_category` int(10) unsigned NOT NULL ,
        `id_products` TEXT  NOT NULL,
        `order` varchar(125),
       PRIMARY KEY  (`id`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';
$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->filterProducts_table . '_filters` (
        `id` int unsigned not null auto_increment,
        `id_filter_products` int(10) unsigned NOT NULL,
        `id_group` int(10) unsigned NOT NULL ,
        `from` int  NOT NULL,
        `to` int  NOT NULL,
        `attributes` VARCHAR(128)  NULL,
        PRIMARY KEY  (`id`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';
//$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->filterProducts_table . '_order` (
//
//        `id_filter_products` int(10) unsigned NOT NULL,
//        `id_product` int(10) unsigned NOT NULL ,
//        `order` int  NOT NULL,
//
//
//) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';
foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
