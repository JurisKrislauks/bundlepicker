/**
 * Created by jkris on 5/30/2017.
 */
$(document).ready(function () {

    // Getter
    // var themeClass = $( ".sort-me" ).sortable( "option", "classes.ui-sortable" );


    $('#filter-category-selector a').click(function () {
        var id_category = $(this).data('catid');
        var id_bundlefilter = $('#filter-body').data('id');
        $('#filter-category-selector a').removeClass('selected');
        $(this).addClass('selected');
        $.ajax(
            {
                url: this.href + '&ajax=1&getCategoryinfo&catID=' + id_category + '&id_bundlefilter=' + id_bundlefilter,
                context: this,
                dataType: 'json',
                cache: 'false',
                success: function (res) {
                    console.log(res);
                    var result = res
                    // $('#indexing-warning').hide();
                    $('#ajax-container .products').html(res.a);


// Setter
                    $(".sort-me").sortable({

                        update: function (event, ui) {

                            var id_category = $(this).parents('.filter-product-cat').data('catid');
                            setOrder(this, "#product-order-" + id_category);
                        }
                    });
                    // if (type == 'price')
                    //     $('#ajax-message-ok productOrder').html(translations['url_indexation_finished']);
                    // else
                    //     $('#ajax-message-ok span').html(translations['attribute_indexation_finished']);
                    //
                    //  $('#ajax-message-ok').show();
                    return;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                    var err = XMLHttpRequest.responseText;

                    console.log(err);
                }
            }
        );
    });
});
var sort = function (id_category) {


    var sortableList = $('#filter-product-selection-' + id_category);
    var listitems = $('li', sortableList);
    var selected = $('#manufacturer-selector-' + id_category + ' option:selected').val();

    listitems.sort(function (a, b) {

        // return ($(a).children('.manufacturer').data('id') ==selected?-1:( $(a).children('.manufacturer').data('id')> $(b).children('.manufacturer').data('id'))) ? 1 : -1;
        return $(a).children('.manufacturer').data('id') != selected;

    });

    sortableList.append(listitems);
    setOrder($('#filter-product-selection-' + id_category), "#product-order-" + id_category);
}
var setOrder = function (elem, target) {
    var productOrder = $(elem).sortable('toArray', {attribute: 'data-prodID'}).toString();

    console.log(productOrder);
    $(target).val(productOrder);
};
var saveConf = function () {
    $('#ajax-message-ok').hide();
    var id_bundlefilter = $('#filter-body').data('id');
    var id_category = $(this).data('catid');
    var id_products = $("#filter-product-selection-" + id_category + " input:checked").map(function () {
        return $(this).val();
    }).get();

    var url = $(this).data('href');

    var dataObject = $('#form-' + id_category).serialize();
    console.log(dataObject);
    $('#filter-body').addClass('loading');
    $.ajax(
        {
            url: url + '&ajax=1&saveCategoryinfo&catID=' + id_category + '&id_bundlefilter=' + id_bundlefilter + '&' + dataObject,
            context: this,
            dataType: 'json',
            cache: 'false',
            data: {'id_products': id_products, 'groups': 1},
            success: function (res) {
                console.log(res);
                var result = res;
                // $('#indexing-warning').hide();
                // $('#ajax-container .products').html(res.a);
                //
                // if (type == 'price')
                //     $('#ajax-message-ok span').html(translations['url_indexation_finished']);
                // else
                //     $('#ajax-message-ok span').html(translations['attribute_indexation_finished']);
                //
                $('#ajax-message-ok').show();
                return;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
                var err = XMLHttpRequest.responseText;

                console.log(err);
            },
            complete: function () {
                $('#filter-body').removeClass('loading');
            }
        }
    );
}