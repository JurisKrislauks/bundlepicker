
{foreach $categories as $category}
    <div class="ajax-category">
        {if $category.enabled}
            <div>{$category.name}</div>
            <select name="ajax-category-select-{$category.id}">
                {foreach $category.products as $product}
                    {if !$product['deleted']}
                        {$product.name}
                        {if isset($product['attrgroups']['combinations'])}
                            {foreach $product['attrgroups']['combinations'] as $id => $combination}

                                {*{$combination.attributes_values|@var_dump}*}
                                {if $combination.enabled}
                                    {*{$combination.attributes_values|@var_dump}*}
                                    <option value="{$id}">{$combination.name}</option>
                                {/if}
                            {/foreach}
                        {/if}
                    {/if}

                    {*</div>*}


                {/foreach}
            </select>
        {/if}

    </div>
{/foreach}