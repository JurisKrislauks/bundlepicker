<div id="ajax-message-ok" style="display: none;">Success</div>
Products
{if !$childs}
    {if isset($products) && sizeof($products)>0}
        <div class="filter-product-cat" data-catid="{$catID}">
            <ul id="filter-product-selection-{$catID}" style="border:1px solid black" class="sort-me col-md-4">

                {foreach $products as $product}
                    <li data-prodID="{$product.id_product}"> {$product.id_product} <input type="checkbox"
                                                                                          value="{$product.id_product}"
                                                                                          name="selectedProducts[]"
                                                                                          class="{if isset($product.checked) && $product.checked}selected{/if}"
                                                                                          {if isset($product.checked) && $product.checked}checked="checked"{/if}>{$product.name}
                        | <span class="manufacturer"
                                data-id="{$product.manufacturer_info.id}">{$product.manufacturer_info.name}</span>
                    </li>
                {/foreach}
            </ul>
            {if isset($manufacturers) && sizeof($manufacturers) > 0}
                <div class="sort-options col-md-6">Sort by manufacturer:
                    <select id="manufacturer-selector-{$catID}">
                        {foreach $manufacturers as $mid=> $manufacturer}
                            <option id="manu-{$mid}" value="{$mid}">{$manufacturer}</option>
                        {/foreach}

                    </select>
                    <button data-catID="{$catID}" data-type="manu"
                            onclick="sort.bind(this)({$catID},)">{l s='Sort' mod='bundlepicker'}
                    </button>
                </div>
            {/if}
            <form id="form-{$catID}" action="" method="post">
                <input type="hidden" name="order" id="product-order-{$catID}" value="{$order}"/>
                <div id="filter-product-filters-{$catID}">
                    <ul>
                        {foreach $combination_groups as $id=>$filter}
                            <li data-id="{$id}">
                                <span>{$filter.name}</span>
                                {*<span>{l s='From' mod='bundlepicker'}:*}
                                <input type="hidden" name="from[{$id}] "
                                       value="{$filter.value.from}"></span>
                                {*<span>{l s='To' mod='bundlepicker'}:*}
                                <input type="hidden" name="to[{$id}] "
                                       value="{$filter.value.to}"></span>
                                <span>Attributes:
                                    {foreach $filter.value.all_attributes as $aid=>$attribute}
                                        <input type="checkbox" name="attribute[{$id}][{$aid}]" value="{$aid}"
                                               {if in_array($aid,$filter.value.selected_attr)}checked="checked"{/if}>
                                        <label>{$attribute}</label>
                                    {/foreach}
                            </span>
                            </li>
                        {/foreach}

                    </ul>
                </div>
            </form>
            <button id="saveConf" data-href="{$ajax_url_id_bundlefilter}" data-catID="{$catID}"
                    onclick="saveConf.bind(this)()">
                Save
            </button>
        </div>
    {else}
        {l s='No products in this category' mod='bundlepicker'}
    {/if}
{else}
    {foreach $data as $catID=>$cate}
        <div class="filter-product-cat row" data-catid="{$catID}">
            <h4>{$cate.catName}</h4>
            {if isset($cate.products) && sizeof($cate.products)>0}
                <ul id="filter-product-selection-{$catID}" class="sort-me col-md-4" style="border:1px solid black">

                    {foreach $cate.products as $product}
                        <li data-prodID="{$product.id_product}">{$product.id_product} <input type="checkbox"
                                                                                             value="{$product.id_product}"
                                                                                             name="selectedProducts[]"
                                                                                             class="{if isset($product.checked) && $product.checked}selected{/if}"
                                                                                             {if isset($product.checked) && $product.checked}checked="checked"{/if}>{$product.name}
                            | <span class="manufacturer"
                                    data-id="{$product.manufacturer_info.id}">{$product.manufacturer_info.name}</span>

                        </li>
                    {/foreach}
                </ul>
                {if isset($cate.manufacturers) && sizeof($cate.manufacturers) > 0}
                    {$cate.manufacturers|@var_dump}
                    <div class="sort-options col-md-6">Sort by manufacturer: <select
                                id="manufacturer-selector-{$catID}">
                            {foreach $cate.manufacturers as $mid=> $manufacturer}
                                <option id="manu-{$mid}" value="{$mid}">{$manufacturer}</option>
                            {/foreach}

                        </select>
                        <button data-catID="{$catID}" data-type="manu"
                                onclick="sort.bind(this)({$catID},)">{l s='Sort' mod='bundlepicker'}
                        </button>
                    </div>
                {/if}
                <div class="col-md-12">
                    <form id="form-{$catID}" action="" method="post">
                        <input type="hidden" name="order" id="product-order-{$catID}" value="{$cate.order}"/>
                        <div id="filter-product-filters-{$catID}">
                            <ul>
                                {foreach $cate.combination_groups as $id=>$filter}
                                    <li data-id="{$id}">
                                        <span>{$filter.name}</span>
                                        <span>{l s='From' mod='bundlepicker'}:<input type="number" name="from[{$id}] "
                                                                                     value="{$filter.value.from}"></span>
                                        <span>{l s='To' mod='bundlepicker'}:<input type="number" name="to[{$id}] "
                                                                                   value="{$filter.value.to}"></span>
                                        <span>Attributes:
                                            {foreach $filter.value.all_attributes as $aid=>$attribute}
                                                <input type="checkbox" name="attribute[{$id}][{$aid}]" value="{$aid}"
                                                       {if in_array($aid,$filter.value.selected_attr)}checked="checked"{/if}>
                                                <label>{$attribute}</label>
                                            {/foreach}
                            </span>
                                    </li>
                                {/foreach}

                            </ul>
                        </div>
                    </form>
                </div>
                <button id="saveConf-{$catID}" data-href="{$cate.ajax_url_id_bundlefilter}" data-catID="{$catID}"
                        onclick="saveConf.bind(this)()">Save
                </button>
            {else}
                {l s='No products in this category' mod='bundlepicker'}
            {/if}
        </div>
    {/foreach}
{/if}