
<div id="bundlepicker-ajax" class="hidden" data-href="{$link->getModuleLink('bundlepicker', 'category')}"></div>
{foreach $subcategories as $subc}
    <div class="subcategory col-md-4" data-id="{$subc.id_bundlepicker}">
        {$subc.lang.name}
        {if isset($subc.filters)}
            {foreach $subc.filters as $filter}
                {*{$filter|@var_dump}*}
                <div class="filter" data-id="{$filter.id_bundlefilter}">

                    {$filter.name} <input type="number" class="filter-selector"
                                          name="bundlef[{$filter.id_bundlefilter}]" data-id="{$filter.id_bundlefilter}"
                                          placeholder="{l s='Val' mod='bundlepicker'}">
                </div>
            {/foreach}
        {/if}

    </div>
{/foreach}
<div class="clearfix"></div>


{foreach $subcategories as $subc}
    <div id="ajax-load-{$subc.id_bundlepicker}"></div>
{/foreach}
