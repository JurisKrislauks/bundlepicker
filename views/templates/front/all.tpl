<div class="block_projects">
    <div class="block_title">{l s='Projekti, kuros izmantoti mūsu materiāli' mod='bundlepicker'}</div>
    <div class="sub_title">{l s='PAROC Akmens vate eXtra Akmens vate eXtra' mod='bundlepicker'}</div>
    <div class="block_content big">
        <div class="group_wrap">
            {foreach from=$bundlepicker item=icon key=iconid name=icons}
                <div class="views_row simple icon_{$iconid}">
                    <img class="product-icon-image img-circle" src="{$icon.image}" alt="iconimage">
                    <div class="title_wrap">
                        <div class="title">{$icon.name}</div>
                        <a href="{if $ajax}#product-decription-{$iconid}{else}{$link->getModuleLink('bundlepicker','category',['process'=>'view','id'=>{$iconid}])}{/if}"
                           target='_blank' class="product-icon-link view_more" data-id="{$iconid}">
                           {l s='More' mod='bundlepicker'}
                        </a>
                    </div>
                </div>
            {/foreach}

        </div>

    </div>
    {*<div class="block_footer">*}
        {*<a href="{$link->getModuleLink('bundlepicker','project',['process'=>'all'])}" class="form_submit"><span> </span> Skatīt vairāk</a>*}
    {*</div>*}
</div>
{* The popup *}
<div class="product-icons-popup" id="product-icons-popup">
    <div class="product-icons-popup-wrapper">
        <button class="product-icons-popup-close">
            <i class="icon-times"></i>
        </button>
        <div class="product-icons-popup-content">
        </div>
    </div>
</div>