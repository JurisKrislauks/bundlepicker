<div id="filter-body" data-id="{$id_bundlefilter}">
    <div class="col-md-3">
        {foreach $categories as $category}
            <div id="filter-category-selector">
                <a href="{$ajax_url_id_bundlefilter}" onclick="return false;"
                   data-catid="{$category->id}">{$category->name}</a>

            </div>
        {/foreach}
    </div>
    <div class="col-md-9">
        <div id="ajax-container">
            <div class="products">

            </div>
        </div>
    </div>
</div>